db.users.find({
	$and: [
		{
			department: "HR"
		},
		{
			age: {
				$gte: 70
			}
		}
	]
});

db.users.find({
	$and: [
		{
			firstName: { $regex: 'e', $options: '$i' }
		},
		{
			age: {
				$lte: 30
			}
		}
	]
});
